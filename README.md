[![GoDoc](https://godoc.org/blitter.com/go/cryptmt?status.svg)](https://godoc.org/blitter.com/go/cryptmt)

Implementation of cryptMTv1 stream cipher (but with mtwist64 as base accum) 
https://eprint.iacr.org/2005/165.pdf 

Uses Mersenne Twister 64 golang implementation supplied by [cuixin](https://gist.github.com/cuixin): [gist](https://gist.github.com/cuixin/1b8b6bd7bfbde8fe76e8)
