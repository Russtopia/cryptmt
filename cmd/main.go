// WANDERER - a crypto doodle that appears to give adequate
// protection to data in a stream cipher context (?)
//
// Properties visualized using https://github.com/circulosmeos/circle
//
// test command-line 'main' program
package main

// TODOs:
// -use a crypto rand (eg mtwist64) instead of go pkg rand?
// -define s-box rotation/shuffle schema
// -devise p-box schema
// ...

import (
	"flag"
	"io"
	"os"

	"blitter.com/go/cryptmt"
)

var (
	k string
)

func main() {
	flag.StringVar(&k, "k", "WARNING_DEFAULT_KEY", "Key (NOTE insecure specified on command line)")
	flag.Parse()

	c := cryptmt.New(os.Stdin, os.Stdout, []byte(k))
	_, _ = io.Copy(c, c)
}
